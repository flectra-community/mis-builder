# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "MIS Builder Import / Export",
    "summary": """Import / Export Reports with all dependencies""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Jamotion GmbH",
    "website": "https://gitlab.com/flectra-community/mis-builder",
    "depends": ["mis_builder_expimp", "mis_builder_budget"],
    "data": [],
    "installable": True,
    "auto_install": True,
}
