# Copyright 2014 ACSONE SA/NV (<http://acsone.eu>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
import base64
import json

from lxml import etree

from flectra import api, fields, models, _


class MisBuilderExportWizard(models.TransientModel):
    _inherit = "mis.builder.export.wizard"

    @api.model
    def _get_query_fields(self):
        result = super(MisBuilderExportWizard, self)._get_query_fields()
        return result

    @api.model
    def _get_report_fields(self):
        result = super(MisBuilderExportWizard, self)._get_report_fields()
        return result

    @api.model
    def _get_kpi_fields(self):
        result = super(MisBuilderExportWizard, self)._get_kpi_fields()
        result.append('budgetable')
        return result

    @api.model
    def _get_style_fields(self):
        result = super(MisBuilderExportWizard, self)._get_style_fields()
        return result
