# Flectra Community / mis-builder

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[mis_builder_budget](mis_builder_budget/) | 2.0.4.0.0|         Create budgets for MIS reports
[mis_builder](mis_builder/) | 2.0.4.0.0|         Build 'Management Information System' Reports and Dashboards    
[mis_builder_demo](mis_builder_demo/) | 2.0.3.1.3|         Demo addon for MIS Builder


