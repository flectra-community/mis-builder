# Copyright 2014 ACSONE SA/NV (<http://acsone.eu>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
import base64
import json

from lxml import etree

from flectra import api, fields, models, _

QUERY_FIELDS = ['name', 'model_id', 'field_ids', 'aggregate', 'date_field', 'query_context', 'domain']

REPORT_FIELDS = ['name', 'description', 'account_model', 'subreport_ids']

KPI_FIELDS = [
    'name', 'description', 'multi', 'auto_expand_accounts', 'style_expression',
    'type', 'compare_method', 'accumulation_method', 'sequence',
    'expression'
]

STYLE_FIELDS = [
    'name',
    'color_inherit', 'color',
    'background_color_inherit', 'background_color',
    'font_style_inherit', 'font_style',
    'font_weight_inherit', 'font_weight',
    'font_size_inherit', 'font_size',
    'indent_level_inherit', 'indent_level',
    'prefix_inherit', 'prefix',
    'suffix_inherit', 'suffix',
    'dp_inherit', 'dp',
    'divider_inherit', 'divider',
    'hide_empty_inherit', 'hide_empty',
    'hide_always_inherit', 'hide_always',
]


class MisBuilderExportWizard(models.TransientModel):
    _name = "mis.builder.export.wizard"
    _description = "Export MIS Builder Report"

    report_id = fields.Many2one(
            comodel_name='mis.report',
            string='Report',
            required=True,
    )

    name = fields.Char(
            string='File Name',
            default='mis_report.json',
    )

    file_save = fields.Binary(
            string='Settings File',
            readonly=True,
    )

    state = fields.Selection([
        ('draft', 'Draft'),
        ('download', 'Download')
    ], default='draft')

    def export(self):
        self.ensure_one()

        report_data = self._read_subreport_data(self.report_id.subreport_ids)
        report_data.append(self._read_report_data(self.report_id))

        json_data = json.dumps(report_data, indent=2)
        # change state of the wizard
        self.write({
            'name': '%s.json' % self.report_id.name,
            'file_save': base64.b64encode(json_data.encode()),
            'state': 'download'
        })

        return {
            'name': _('Save'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': self._name,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id,
        }

    @api.model
    def _read_subreport_data(self, subreport_ids):
        report_data = []
        for subreport_id in subreport_ids:
            report_data.extend(self._read_subreport_data(subreport_id.subreport_id.subreport_ids))
            report_data.append(self._read_report_data(subreport_id.subreport_id))

        return report_data

    @api.model
    def _read_report_data(self, report_id):
        report_data = report_id.read(self._get_report_fields())[0]
        del report_data['id']
        report_data['extid'] = '__export__.mis_report_%s' % report_id.id
        report_data['move_lines_source'] = report_id.move_lines_source.model
        if report_id.style_id:
            report_data['style_id'] = report_id.style_id.read(self._get_style_fields())[0]
            del report_data['style_id']['id']
            report_data['style_id']['extid'] = '__export__.mis_report_style_%s' % report_id.style_id.id
        if report_id.query_ids:
            report_data['query_ids'] = []
            for query_id in report_id.query_ids:
                query_data = query_id.read(self._get_query_fields())[0]
                del query_data['id']
                query_data['model_id'] = query_id.model_id.model
                query_data['field_ids'] = query_id.field_ids.mapped('name')
                query_data['extid'] = '__export__.mis_report_query_%s' % query_id.id
                report_data['query_ids'].append(query_data)
        if report_id.kpi_ids:
            report_data['kpi_ids'] = []
            last_kpi_sequence = -1
            for kpi_id in report_id.kpi_ids.sorted(key=lambda s: s.sequence):
                kpi_data = kpi_id.read(self._get_kpi_fields())[0]
                # Fix of sequences to be unique - otherwise there are problems on importing reports with subkpis
                if kpi_id.sequence <= last_kpi_sequence:
                    kpi_data['sequence'] = last_kpi_sequence + 1
                last_kpi_sequence = kpi_data['sequence']
                del kpi_data['id']
                if kpi_id.style_id:
                    kpi_data['style_id'] = kpi_id.style_id.read(self._get_style_fields())[0]
                    del kpi_data['style_id']['id']
                    kpi_data['style_id']['extid'] = '__export__.mis_report_style_%s' % kpi_id.style_id.id
                if kpi_id.auto_expand_accounts_style_id:
                    kpi_data['auto_expand_accounts_style_id'] = kpi_id.auto_expand_accounts_style_id.read(self._get_style_fields())[0]
                    del kpi_data['auto_expand_accounts_style_id']['id']
                    kpi_data['auto_expand_accounts_style_id'][
                        'extid'] = '__export__.mis_report_style_%s' % kpi_id.auto_expand_accounts_style_id.id
                kpi_data['extid'] = '__export__.mis_report_kpi_%s' % kpi_id.id
                kpi_data['expression_ids'] = []
                for expression_id in kpi_id.expression_ids:
                    expression_data = expression_id.read(['sequence', 'name'])[0]
                    del expression_data['id']
                    expression_data['extid'] = '__export__.mis_report_kpi_expression_%s' % expression_id.id
                    kpi_data['expression_ids'].append(expression_data)
                report_data['kpi_ids'].append(kpi_data)
        if report_id.subkpi_ids:
            report_data['subkpi_ids'] = []
            for subkpi_id in report_id.subkpi_ids:
                subkpi_data = subkpi_id.read(['sequence', 'name', 'description'])[0]
                del subkpi_data['id']
                subkpi_data['extid'] = '__export__.mis_report_subkpi_%s' % subkpi_id.id
                subkpi_data['expression_ids'] = []
                for expression_id in subkpi_id.expression_ids:
                    expression_data = expression_id.read(['sequence', 'name'])[0]
                    del expression_data['id']
                    expression_data['kpi_id'] = '__export__.mis_report_kpi_%s' % expression_id.kpi_id.id
                    expression_data['extid'] = '__export__.mis_report_kpi_expression_%s' % expression_id.id
                    subkpi_data['expression_ids'].append(expression_data)
                report_data['subkpi_ids'].append(subkpi_data)
        if report_id.subreport_ids:
            report_data['subreport_ids'] = []
            for subreport_id in report_id.subreport_ids:
                subreport_data = subreport_id.read(['name'])[0]
                del subreport_data['id']
                subreport_data['subreport_id'] = '__export__.mis_report_%s' % subreport_id.subreport_id.id
                report_data['subreport_ids'].append(subreport_data)
        return report_data

    @api.model
    def _get_query_fields(self):
        return QUERY_FIELDS

    @api.model
    def _get_report_fields(self):
        return REPORT_FIELDS

    @api.model
    def _get_kpi_fields(self):
        return KPI_FIELDS

    @api.model
    def _get_style_fields(self):
        return STYLE_FIELDS
