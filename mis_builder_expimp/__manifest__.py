# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "MIS Builder Import / Export",
    "summary": """Import / Export Reports with all dependencies""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Jamotion GmbH",
    "website": "https://gitlab.com/flectra-community/mis-builder",
    "depends": ["mis_builder"],
    "data": [
        "wizards/mis_builder_export_views.xml",
        "wizards/mis_builder_import_views.xml",
        "security/ir.model.access.csv",
    ],
    "installable": True,
}
